const WebSocket = require("ws");
const pty = require("node-pty");

function startWs() {
	const client = new WebSocket("ws://127.0.0.1:2346/");
	
	var term = {};
	client.on("open", () => {
		client.send(
			JSON.stringify({
				channel: 'gateway_service',
				method: 'clientRegister',
				params: {
					'uuid': 'xterm_server',
				}
			})
		);
	});
	
	client.on("message", jsonMsg => {
		const msg = JSON.parse(jsonMsg);
	
		switch (msg.body.option) {
			case "term_init":
				term[msg.body.from] = pty.spawn("bash", [], {
					name: "xterm-color",
					cwd: process.env.HOME,
					env: process.env
				});
				term[msg.body.from].on("data", function(data) {
					client.send(
						JSON.stringify({
							channel: 'gateway_service',
							method: 'xtermRenderSend',
							params: {
								sendTo: 'xterm_client',
								msg: data
							}
						})
					);
				});
				break;
			case "term_resize":
				term[msg.body.from].resize(msg.body.data.cols, msg.body.data.rows);
				break;
			case "term_cmd":
				term[msg.body.from].write(msg.body.data);
				break;
		}
	});
	
	client.on("close", () => {
		for (let k in term) {
			term[k].kill();
		}
	});
}

startWs();