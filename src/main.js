import Vue from "vue";
import App from "./App.vue";
import router from "./router.js";
import store from "./store/index.js";
import "./plugins/element.js";
import "normalize.css";
import HighchartsVue from 'highcharts-vue';
import VueDraggableResizable from 'vue-draggable-resizable';
import 'vue-draggable-resizable/dist/VueDraggableResizable.css';
import io from './assets/js/socket-io.js';

Vue.use(HighchartsVue);

Vue.component('vue-draggable-resizable', VueDraggableResizable);

Vue.prototype.$socketIO = io;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");
