import Vue from "vue";
import Vuex from "vuex";
import os from "./os.js";
import containers from "./containers.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    os,
    containers
  }
});
