export default {
	namespaced: true,
  state: {
		key: 0,
    windows: [],
  },
  getters: {
		getWindows(state) {
			return state.windows;
		}
	},
  mutations: {
		pushWindow(state, component) {
      let window = {};
      window.component = component;
			window.key = state.key++;
			state.windows.push(window);
		},
    regroupWindowsZindex(state, id) {
      const currentWindow = state.windows[id];
      state.windows.splice(id, 1);
      state.windows.push(currentWindow);
    }
  },
  actions: {
	}
}