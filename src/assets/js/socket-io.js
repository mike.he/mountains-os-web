import io from 'socket.io-client';

const domain = process.env.VUE_APP_WS_HOST;

export default io(`ws://${domain}`);