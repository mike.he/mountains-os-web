import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/term",
      name: "term",
      component: () =>
        import("./components/Term.vue")
    },
    {
      path: "/docker",
      name: "docker",
      component: () =>
        import("./components/Docker.vue")
    },
    {
      path: "/monitor",
      name: "monitor",
      component: () =>
        import("./components/Monitor.vue")
    },
    {
      path: "/",
      name: "login",
      component: () =>
        import("./components/Login.vue")
    },
    {
      path: "/os",
      name: "os",
      component: () =>
        import("./components/Os.vue")
    },
    {
      path: "/bg",
      name: "bg",
      component: () =>
        import("./components/Bg.vue")
    }
  ]
});
